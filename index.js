require('dotenv').config();
require('sqreen');
var express = require('express');
var app = express();
var path = require('path');
var serveStatic = require('serve-static');

app.use('/', express.static('dist', {
    extensions: ['html']
}));

app.listen(8080);